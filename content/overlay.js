// Myostis - a Pale Moon extension to quickly forget about one page
// Copyright (C) 2021-2025 Alessio Vanni

// This file is part of Myostis.

// Myostis is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// Myostis is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.

// You should have received a copy of the GNU General Public License
// along with Myostis.  If not, see <https://www.gnu.org/licenses/>.

'use strict';

var Myosotis = (function () {
    Components.utils.import('resource://gre/modules/PlacesUtils.jsm');

    return {
	forgetPage: function () {
	    let dest = window.gBrowser
		.selectedBrowser
		.contentDocument
		.activeElement;

	    if ('a' !== dest.tagName.toLowerCase()) {
		return;
	    }

	    let domain = dest.href;
	    PlacesUtils.history.removePage(PlacesUtils.toURI(domain));
	},
    }
})();
